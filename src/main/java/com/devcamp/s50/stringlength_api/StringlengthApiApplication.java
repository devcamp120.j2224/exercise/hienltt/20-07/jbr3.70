package com.devcamp.s50.stringlength_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringlengthApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringlengthApiApplication.class, args);
	}

}
