package com.devcamp.s50.stringlength_api;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LengthControl {
    @CrossOrigin
    @GetMapping("/length")
    public int getLength(){ //có đầu ra là int
        String string = "String Length RestAPI"; //đầu vào của request là một chuỗi

        int StringLength = string.length();
        System.out.println("Chiều dài của chuỗi là: " + StringLength);

        return StringLength;
    }
}
